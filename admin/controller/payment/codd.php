<?php
class ControllerPaymentCodd extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/codd');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('codd', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/codd', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('payment/codd', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['codd_total'])) {
			$data['codd_total'] = $this->request->post['codd_total'];
		} else {
			$data['codd_total'] = $this->config->get('codd_total');
		}

		if (isset($this->request->post['codd_order_status_id'])) {
			$data['codd_order_status_id'] = $this->request->post['codd_order_status_id'];
		} else {
			$data['codd_order_status_id'] = $this->config->get('codd_order_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['codd_geo_zone_id'])) {
			$data['codd_geo_zone_id'] = $this->request->post['codd_geo_zone_id'];
		} else {
			$data['codd_geo_zone_id'] = $this->config->get('codd_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['codd_status'])) {
			$data['codd_status'] = $this->request->post['codd_status'];
		} else {
			$data['codd_status'] = $this->config->get('codd_status');
		}

		if (isset($this->request->post['codd_sort_order'])) {
			$data['codd_sort_order'] = $this->request->post['codd_sort_order'];
		} else {
			$data['codd_sort_order'] = $this->config->get('codd_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('payment/codd.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/codd')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}