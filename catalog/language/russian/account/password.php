<?php
// Heading 
$_['heading_title']  = 'Смена пароля';

// Text
$_['text_account']   = 'Личный Кабинет';
$_['text_password']  = 'Смена пароля';
$_['text_success']   = 'Ваш пароль успешно изменен!';

// Entry
$_['entry_password'] = 'Новый пароль';
$_['entry_old_password'] = 'Текущий пароль';
$_['entry_confirm']  = 'Подтвердить пароль';

// Error
$_['error_password'] = 'Пароль должен быть от 4 до 20 символов!';
$_['error_confirm']  = 'Пароли не совпадают!';
$_['error_old_password']  = 'Текущий пароль введен неаерно!';