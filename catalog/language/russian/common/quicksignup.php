<?php
// Text
$_['text_signin']    = 'Авторизация';
$_['text_register']    = 'Регистрация';
$_['text_login']   			  = 'Войти';
$_['button_register']    		  = 'Зарегистрироваться';

$_['text_new_customer']       = 'New Customer';
$_['text_returning']          = 'Returning Customer';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'ФИО';
$_['entry_password']          = 'Пароль';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Забыли пароль?';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';


$_['text_autorization']       = 'Авторизация';
$_['text_show_pass']       = 'Показать пароль';
$_['button_continue']       = 'зарегестрироваться';

//Button
$_['button_login']            = 'Login';

//Error
$_['error_name']           = 'ФИО должно быть от 1 до 32 символов!';
$_['error_lastname']       = 'Фамилия должна быть от 1 до 32 символов!';
$_['error_email']          = 'Адрес электронной почты не действителен!';
$_['error_telephone']      = 'Телефон должен соответствовать маске!';
$_['error_password']       = 'Пароль должен быть от 6 до 20 символов!';
$_['error_exists']         = 'Внимание: E-Mail уже зарегистрирован!';
$_['error_agree']          = 'Warning: You must agree to the %s!';
$_['error_warning']        = 'Warning: Please check the form carefully for errors!';
$_['error_approved']       = 'Warning: Your account requires approval before you can login.';
$_['error_login']          = 'Предупреждение: нет соответствия для адреса электронной почты или пароля.';