function searchwidth() {
    var ml = $('.wrapper-menu').width();
    aw = ml + 150;
    $('#search .search_block_content').css("width", aw);
    $('#search .search_block_content').css("left", -aw + 53);
}

// функция узнает размер окна браузера, и задает её для блока div
function fullHeight(){
    $('.error-wrapper').css({
        height: $(window).height() + 'px'
    });
}

var h_hght = 145; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна

$(function(){

    var elem = $('header .mini-position');
    var top = $(this).scrollTop();

    if(top > h_hght){

        elem.css('top', h_mrg);
    }
    if ($(window).width() <= 767) {
        $(window).scroll(function(){
            top = $(this).scrollTop();

            if (top+h_mrg < h_hght) {
                elem.removeClass('fixed');
                elem.css('top', (h_hght-top));
            } else {
                elem.addClass('fixed');
                elem.css('top', h_mrg);
            }
        });
    }

});
$(window).resize( function() {
    fullHeight();
    if ($(window).width() <= 767) {
        $('.js-position-menu').appendTo('.xs-menu-block');
    } else{
        $('.js-position-menu').appendTo('.mini-position');
    }
});
$(window).load( function() {

    $(".ninja-btn").click( function() {
        $(".ninja-btn, .panel-overlay, .panel").toggleClass("active");
        /* Check panel overlay */
        if ($(".panel-overlay").hasClass("active")) {
            $(".panel-overlay").addClass('active-overlay');
        } else {
            $(".panel-overlay").removeClass('active-overlay');
        }
    });

});
$(window).on("load resize", function() {
    var menuHeightOffset = $(".panel").find("ul").height() /2;

    $(".panel").find("ul").css({
        "margin-top": -menuHeightOffset
    });
    $('select.form-control').select2({
        minimumResultsForSearch: Infinity
    });

});
function productImageSlider() {
    if ($('*').is('.thumbnails')) {

        $('.thumbnails').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.thumbnails-mini'
        });
        $('.thumbnails-mini').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.thumbnails',
            dots: false,
            arrows: false,
            speed: 200,
            centerMode: false,
            vertical: true,
            focusOnSelect: true,
            centerPadding: '40px',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        autoplay: true
                    }
                }
            ]
        });


        $('#lightgallery').lightGallery({
            loop: true,
            fourceAutoply: true,
            thumbnail: true,
            hash: false,
            speed: 400,
            scale: 1,
            keypress: true,
            counter: false,
            download: false,
        });
        $('.click-triger-zoom').on('click', function (e) {
            $('.thumbnails .slick-current img').data("link");
            $('#' + $('.thumbnails .slick-current img').data("link")).trigger("click");
        });
    }
}
$(document).ready(function () {
    $( '.slide_wrapper_big' ).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        arrows: false,
        dots: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-left-chevron" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron" aria-hidden="true"></i></button>',
    });
    $('.slider').each( function() {
        $( this ).slick({
            infinite: false,
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-left-chevron" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
    });
    fullHeight();
    searchwidth();
    productImageSlider();
    $(window).bind("resize", function() {
        searchwidth();
    });

    // $(".dropdown-list .list-notactive").attr("data-count", "0");
    // $(".dropdown-list .list-notactive").on("click", function() {
    //     var clickCount = $(this).attr("data-count");
    //     clickCount++;
    //     if (clickCount == 1) {
    //         $(".categoriesPage .categoryBox .textBox[data-count='1']").attr("data-count", "0");
    //         $(this).attr("data-count", clickCount);
    //         return false;
    //     } else {
    //         return true;
    //     }
    // });


    $('.humburger_wrapp').click(function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('body').removeClass('open_main_menu')
            $('.main_menu_wrapp').slideUp();
            return false;
        }
        $(this).addClass('active');
       $('body').addClass('open_main_menu')
       $('.main_menu_wrapp').slideDown();
    });


    $('body').on('click', function() {
        $('.search_container').removeClass('open');
    });
    $('.search_container').on('click', function(e) {
        e.stopPropagation();
    });
    $('.search_container .header_search').on('click', function(e) {
        e.stopPropagation();
        $('.search_container').toggleClass('open');
    });
    $('.header_search').mouseover(function() {
        $('.search_container').addClass('open');
    });
    $('#input-search').mouseleave(function() {
        if (!$('#input-search').is(':focus')) {
            $('.search_container').removeClass('open');
        }
    });
    $('.search_container').mouseleave(function() {
        if (!$('#input-search').is(':focus')) {
            $('.search_container').removeClass('open');
        }
    });




    if ($(window).width() <= 767) {
        $('.js-position-menu').appendTo('.xs-menu-block');
    } else{
        $('.js-position-menu').appendTo('.mini-position');
    }
    // $('.simplecheckout-button-block.buttons').appendTo('.btn-simplecheckout');

    $('button.wishlist-js').click(function () {
        $(this).attr('disabled', true);
    });

    var url = document.location.href;
    $.each($(".color-radio a"), function() {
        if (this.href == url) {
            $(this).parent().addClass('activeCSS');
        }
    });

    $('.js-information-menu').click(function (e) {
        $('body, html').toggleClass('overflow-body');
        $('.top').toggleClass('open');
        $(this).toggleClass('active');
        $('.wrapper-top-menu .list-inline').toggleClass('active');
    });

    $('.js-catalog-menu').click(function (e) {
        $('.navbar').toggleClass('open');
    });

    $(".in_product").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
        helpers : {
            title : {
                type : false
            },
            overlay: {
                locked: false
            }
        }
    });

    $('.mfilter-box .box-heading').click(function () {
        $(this).toggleClass('open');
        $('.mfilter-content').toggleClass('open');
    });


    /*$('.qty_block .button_plus').click(function () {
        var num = $('.qty_block input[name=quantity]').val();
        var min = parseInt($('.qty_block input[name=quantity]').data('min'));
        $('.qty_block input[name=quantity]').val(parseInt(num)+parseInt($('.qty_block input[name=quantity]').data('min')));
    });
    $('.qty_block .button_minus').click(function () {
        if($('.qty_block input[name=quantity]').val() > $('.qty_block input[name=quantity]').data('min')) {
            var num = $('.qty_block input[name=quantity]').val();
            $('.qty_block input[name=quantity]').val(parseInt(num) - parseInt($('.qty_block input[name=quantity]').data('min')));
        }
    });*/


    $('.js-toggle-main-menu').click(function(){
        $(this).toggleClass('active');
    });
    $('.text-share').click(function(){
        $('.share_wrapp').toggleClass('active');
    });
    $(".filter-sm").click(function(e){
        e.preventDefault();
        if ($(window).width() <= 991) {
            $("#column-left").toggleClass("open-filter");
            $(this).toggleClass("open");
        } else {
        }
    });
    $(".wishlist-js").on('click', function(){
        $(this).addClass('active').prop('disabled', true);
        $(this).html('<i class="icon-heart" aria-hidden="true"></i>');
    });

    var url = document.location.href;
    $.each($(".active-menu li.menu-attr a"), function() {
        if (this.href == url) {
            $(this).parent().addClass('activeCSS');
        };
    });


    $('button.in_wishlist').click(function () {
        $(this).attr('disabled', true);
    });

    var countLi = $('.mfilter-content ul li').size();
    if(countLi > 2){
        $('.mfilter-openall a').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                content = $('.mfilter-content ul');

            if(!$this.hasClass('trigger')){
                $this.addClass('trigger');
                $this.html('Скрыть <i class="fa fa-angle-up" aria-hidden="true">');
                content.addClass('open');
            } else {
                $this.removeClass('trigger');
                $this.html('Показать все <i class="fa fa-angle-down" aria-hidden="true">');
                content.removeClass('open');
            }
        });
    } else {
        $('.mfilter-openall').hide();
    }

    $('input[type=tel], input[name="phone"], #customer_telephone, input[name="telephone"]').mask('(999) 999-99-99');

    $('select.form-control').select2({
        minimumResultsForSearch: Infinity
    });

    $("input[name=quantity]").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $("#input-amount").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $('.js-height').matchHeight({
        property: 'height'
    });

    if ($(window).width() <= 991) {
        $('.text__news .description').dotdotdot({
            height: 80
        });
    }

    $('input, textarea').focus(function () {
        $(this).closest('.has-error').find('.simplecheckout-error-text').remove();
        $(this).closest('.has-error').find('.text-danger').remove();
        $(this).closest('.has-error').removeClass('has-error');
    });

    $('input, textarea').focus(function () {
        $(this).closest('form').find('.alert-danger').remove();
    });

    $('.form-style input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });
    $('.form-group input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });

    $('.form-group textarea').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });
    $(document).on('click', '.btn-simple button', function () {
        $('#'+$(this).data('ids')).trigger('click');
    });
    $('.show_pass_ch input').change(function () {
        if($(this).is(':checked')){
            $(this).parent().parent().parent().parent().find('input[name=password]').attr('type','text');
        }else{
            $(this).parent().parent().parent().parent().find('input[name=password]').attr('type','password');

        }
    });

    $('.dp_block > div').matchHeight();
    $('.how_to_order .order_item').matchHeight();



    function get_popup_purchase(product_id) {
        var url = 'index.php?route=module/popup_purchase&product_id=' + product_id;
        if (url.indexOf('#') == 0) {
            $(url).modal('open');
        } else {
            $.get(url, function (data) {
                $('' + data + '').modal();
            }).success(function () {
                // $('input:text:visible:first').focus();
            });
        }
    }

    $(document).on('click', '.cart_button_wrapp button', function () {
        $('#'+$(this).data('ids')).trigger('click');
    });


    if($('.controll_contaien').outerWidth() == 1){
        $('.html-module__notborder .container').slick({
            dots: true,
            arrows: true,
            prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-left-chevron" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron" aria-hidden="true"></i></button>',
        });

        $(".footer-title").click(function(e){
            e.preventDefault();
            var $parentBlock = $(this).parent('.parent-elements');
            $parentBlock.toggleClass('open');
            $parentBlock.find('.hide_xs, #newsletter').slideToggle(500);
            $(this).toggleClass("open");
        });
    }

});
$(document).mouseup(function (e) {
    var container = $(".colorSelector"),
        container2 = $(".colorSelector2");

    if (container.has(e.target).length === 0){
        container.hide();
    }
    if (container2.has(e.target).length === 0){
        container2.hide();
    }
});
