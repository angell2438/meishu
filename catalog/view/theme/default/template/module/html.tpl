<div class="html-module <?php if($main_id) { ?>html-module__padding<?php } ?>">
    <div class="container">
        <?php if($heading_title) { ?>
            <h3><?php echo $heading_title; ?></h3>
        <?php } ?>

        <?php echo $html; ?>
    </div>

</div>
