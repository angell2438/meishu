<div class="module-product">
    <div class="container">
        <h3><?php echo $heading_title; ?></h3>
        <div class="row">
            <div class="slider" data-slick='{"arrows": true}'>
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module',
                        array(
                            'product' => $product,
                            'button_cart' => $button_cart,
                            'text_tax' => $text_tax,
                            'button_wishlist' => $button_wishlist,
                            'by_one_click' => $by_one_click,
                            'button_compare' => $button_compare));?>
                <?php } ?>
            </div>
        </div>

    </div>
</div>


