<div class="module-product module-product__nonborder ">
    <div class="container">
        <div class="row">
            <h3><?php echo $heading_title; ?></h3>
            <div class="slider" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": true}'>
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module',
                        array(
                            'product' => $product,
                            'button_cart' => $button_cart,
                            'text_tax' => $text_tax,
                            'button_wishlist' => $button_wishlist,
                            'by_one_click' => $by_one_click,
                            'button_compare' => $button_compare));?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


