<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">

    <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <?php  if ($categories && $count_parts <= 1) { ?>
            <h1 class="title-h1"><?php echo $heading_title; ?></h1>
            <?php if ($thumb) { ?>
                <div class="image-category">
                    <img src="<?php echo $thumb; ?>"
                         alt="<?php echo $heading_title; ?>"
                         title="<?php echo $heading_title; ?>"  />
                </div>
            <?php } ?>
            <div class="all-category clearfix">
                <div class="row">
                    <?php foreach ($categories as $category) { ?>
                        <div class="col-md-12 container-for-marg">
                            <div class="category-page_items">
                                <div class="category-info" style="background-image: url(<?php echo $category['thumb']; ?>);">
                                    <div class="name">
                                        <?php echo $category['name']; ?>
                                    </div>
                                    <div class="category-description-mini">
                                        <?php echo $category['description_mini']; ?>
                                    </div>
                                    <a href="<?php echo $category['href']; ?>">посмотреть все</a>
                                </div>

                                <img src="<?php echo $category['thumb']; ?>" alt=" <?php echo $category['name']; ?>">

                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
      <?php if ($products) { ?>

          <h1 class="title-h1"><?php echo $heading_title; ?></h1>
          <div class="flex-row flex-colum-xxs sort-select">
                      <p class="text-sort"><?php echo $text_sort; ?></p>
                      <div class="wrapp_links flex-row">
                          <?php foreach ($sorts as $sort) { ?>
                              <div class="sort_item<?php echo ($sort['href'] == $lsd)? ' is_active':''; ?>" >
                                  <a <?php echo !($sort['href'] == $lsd)? 'href="'.$sort['href'].'"':''; ?>><?php echo $sort['text']; ?></a>
                              </div>
                          <?php } ?>
                      </div>
                  </div>

      <div class="row">
        <?php foreach ($products as $product) { ?>
            <?php $this->partial('product_item',
                array(
                    'product' => $product,
                    'button_cart' => $button_cart,
                    'text_tax' => $text_tax,
                    'button_wishlist' => $button_wishlist,
                    'by_one_click' => $by_one_click,
                    'button_compare' => $button_compare));?>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-12"><?php echo $pagination; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
