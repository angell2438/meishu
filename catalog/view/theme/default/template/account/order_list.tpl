<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <div class="simple-content">
                    <div class="row">
                        <div class="account_title">
                            <h1><?php echo $text_account; ?></h1>
                        </div>
                        <div class="col-md-4">
                            <ul class="account_left_bar">
                                <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                                <li><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                                <li class="active"><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                                <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
                            </ul>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="order_wrapp">
                            <?php if ($orders) { ?>
                                <?php foreach ($orders as $order) { ?>
                                    <div class="item-order">
                                        <p><?php echo $column_order_id . ' ' . $order['order_id'] ?></p>
                                        <div class="order-table">
                                            <div class="header-table flex-row">
                                                <div><?php echo $column_name ?></div>
                                                <div><?php echo $column_quantity ?></div>
                                                <div><?php echo $column_price ?></div>
                                                <div><?php echo $column_status ?></div>
                                            </div>
                                            <div class="content-table">
                                                <div class="flex-row">
                                                    <div class="flex-table">
                                                        <?php foreach ($order['products'] as $key => $product) { ?>
                                                            <div class="table-row flex-row">
                                                                <div><?php echo $product['name']; ?></div>
                                                                <?php if($order['order_status_id'] == 1 || $order['order_status_id'] == 14){ ?>
                                                                    <div class="qty_block">
                                                                        <div class="wrapp_inputs">
                                                                            <div class="button button_minus">
                                                                                <i class="icon-left-chevron"></i>
                                                                            </div>
                                                                            <input type="text" name="product[<?php echo $key ?>][quantity]" value="<?php echo $product['quantity']; ?>" data-min="0" data-product_id="<?php echo $product['product_id']; ?>" size="2"/>
                                                                            <div class="button button_plus">
                                                                                <i class="icon-right-chevron"></i>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" name="key" value="<?php echo $key; ?>"/>
                                                                        <input type="hidden" name="order_id" value="<?php echo $order['order_id']; ?>"/>
                                                                        <input type="hidden" name="product[<?php echo $key ?>][product_id]" value="<?php echo $product['product_id']; ?>"/>
                                                                        <input type="hidden" name="product[<?php echo $key ?>][product_price]" value="<?php echo $product['price']; ?>"/>
                                                                        <input type="hidden" name="product[<?php echo $key ?>][product_symbol]" value="<?php echo $product['price_symbol']; ?>"/>
                                                                        <input type="hidden" name="product[<?php echo $key ?>][product_total]" value="<?php echo (float)$product['price']*$product['quantity']?>"/>
                                                                    </div>
                                                                <?php }else{ ?>
                                                                    <div><?php echo $product['quantity']; ?></div>
                                                                <?php } ?>
                                                                <div class="price-table">
                                                                    <?php echo $product['price_format']; ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="sum-row table-row flex-row">
                                                            <div><?php echo $column_total; ?></div>
                                                            <div class="js-order-total"><?php echo $order['total']; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="payment-table">
                                                        <div class="payment-info">
                                                            <?php if($order['order_status_id'] == 14 || $order['order_status_id'] == 15){ ?>
                                                                <?php $mas = array(
                                                                    '14' => 'minus',
                                                                    '15' => 'checked',
                                                                );?>
                                                                <img src="/catalog/view/theme/default/image/<?php echo $mas[$order['order_status_id']] ?>.svg" alt="<?php echo $mas[$order['order_status_id']] ?>">
                                                                <p><?php echo $order['status']; ?></p>
                                                            <?php }else{ ?>
                                                                <a href="" class="btn btn-by btn-by__pink"><?php echo $text_pay_now ?></a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php }else{ ?>
                                <p><?php echo $text_empty; ?></p>
                            <?php } ?>
                            </div>
                            <?php /*
                          <?php if ($orders) { ?>
                              <div class="order_histor_list">
                                  <?php foreach ($orders as $order) { ?>
                                      <div class="order_histor_item">
                                          <div class="history_block">
                                              <div class="options">
                                                  <div class="option_item"><?php echo $text_qty; ?>: <?php echo $order['order_quantity']; ?></div>
                                              </div>
                                              <div class="price"><?php echo $order['total']; ?></div>
                                          </div>
                                          <div class="history_block">
                                              <div class="text_status"><?php echo $column_status; ?></div>
                                              <div class="status"><?php echo $order['status']; ?></div>
                                              <div class="date"><?php echo $order['date_added']; ?></div>
                                          </div>
                                          <div class="history_block">
                                              <div class="inside">
                                                  <a href="<?php echo $order['href']; ?>"><?php echo $button_view; ?></a>
                                              </div>
                                          </div>
                                      </div>
                                  <?php } ?>
                              </div>
                          <?php }else{ ?>
                              <p><?php echo $text_empty; ?></p>
                          <?php } ?>
                          <br>
                          <br>
                          <div class="buttons clearfix">
                              <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn  btn-by btn-by__pink"><?php echo $button_continue; ?></a></div>
                          </div>*/ ?>
                        </div>
                    </div>
                </div>
                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
        <br>
    </div>
    <?php echo $footer; ?>
<script type="text/javascript">
    $(document).ready(function() {
        function product_update(e){//оновлення product_total
            var key = e.closest('.qty_block').find('input[name=key]').val();
            var quantity = e.closest('.wrapp_inputs').find('input[name=\'product[' + key +'][quantity]\']').val();
            var price = e.closest('.table-row.flex-row').find('input[name=\'product[' + key +'][product_price]\']').val();
            var symbol = e.closest('.table-row.flex-row').find('input[name=\'product[' + key + '][product_symbol]\']').val();
            var out = quantity * price;
            //e.closest('.table-row.flex-row').find('div.price-table').text(out.toFixed(2) + symbol);
            e.closest('.table-row.flex-row').find('input[name=\'product[' + key + '][product_total]\']').val(out.toFixed(2))
        }
        function total_update(e){//оновлення загальної вартості
            var key = 0;//e.closest('.qty_block').find('input[name=key]').val();
            var symbol = e.closest('.flex-table').find('input[name=\'product[0][product_symbol]\']').val();
            var total = 0;
            e.closest('.flex-table').find('input[name=key]').each(function(n,e) {
                key = 1 * $(this).val();
                total += 1 * $('input[name=\'product[' + key + '][product_total]\']').val();
            });
            //console.log(symbol);
            e.closest('.flex-table').find('div.js-order-total').text(total.toFixed(2) + symbol);
            /*e.closest('.flex-table').find('input[name=product_total]').each(function(n,e) {
                total += 1 * $(this).val();
            });
            e.closest('.flex-table').find('div.js-order-total').text(total.toFixed(2) + symbol)*/
        }
        function update_table(e){
            var data = e.closest('.flex-table').find("input").serialize();
            var url = 'index.php?route=account/order/update_order';
            //console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                datatype: 'json',
                success: function(json){

                }
            });
        }
        /*$('input[name=quantity]').each(function(n,e) {
            product_update($(this));
        });*/
        $('.flex-table').find('input[name=key]').each(function(n,e) {
            var key = 1 * $(this).val();
            var quan = $('input[name=\'product[' + key +'][quantity]\']');
            product_update(quan);
        });
        $('.qty_block .button_plus').click(function () {
            var key =$(this).closest('.qty_block').find('input[name=key]').val();
            var num = $(this).closest('.wrapp_inputs').find('input[name=\'product[' + key +'][quantity]\']').val();
            //console.log(key);
            //var min = 1;
            $(this).parent('.wrapp_inputs').find('input[name=\'product[' + key +'][quantity]\']').val(parseInt(num) + 1);
            product_update($(this));
            total_update($(this));
            update_table($(this));
        });
        $('.qty_block .button_minus').click(function () {
            var key = $(this).closest('.qty_block').find('input[name=key]').val();
            //console.log(key);
            if ($(this).parent('.wrapp_inputs').find('input[name=\'product[' + key +'][quantity]\']').val() > $(this).parent('.wrapp_inputs').find('input[name=\'product[' + key +'][quantity]\']').data('min')) {
                var num = $(this).parent('.wrapp_inputs').find('input[name=\'product[' + key +'][quantity]\']').val();
                $(this).parent('.wrapp_inputs').find('input[name=\'product[' + key +'][quantity]\']').val(parseInt(num) - 1);
                product_update($(this));
                total_update($(this));
                update_table($(this));
            }
        });
    });
</script>
