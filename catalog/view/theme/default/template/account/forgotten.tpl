<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>


<div class="container">

  <div class="row">
      <?php if ($success) { ?>
          <div class="alert alert-success"> <?php echo $success; ?><button type="button" class="close" data-dismiss="alert"></button></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
          <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
      <?php } ?>
      <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        <div class="row">
            <?php echo $content_top; ?>
            <div class="col-sm-12">

            <h1 class="hidden"><?php echo $heading_title; ?></h1>

            <div class="wrapper-margin">
                <p><?php echo $text_email; ?></p>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <div class="row">
                                <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="buttons clearfix">
                        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><span><?php echo $button_back; ?></span></a></div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-default"><span><?php echo $button_continue; ?></span></button>
                        </div>
                    </div>
                </form>
            </div>
            </div>

            <?php echo $content_bottom; ?>
        </div>
    </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>