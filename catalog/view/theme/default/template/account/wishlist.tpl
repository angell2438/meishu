<?php echo $header; ?>
<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
<?php } ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                    <div class="row">
                        <?php if ($products) { ?>
                            <?php foreach ($products as $product) { ?>
                                <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12 col-xxs-12">
                                    <div class="product-thumb transition">
                                        <a class="mini-btn-close" href="<?php echo $product['remove']; ?>" ><i class="icon-close"></i></a>
                                        <div class="image">
                                            <div class="sale-label">
                                                <?php if ($product['sales']) { ?>

                                                    <?php if ($product['count_end_date']) { ?>
                                                        <div class="timer-curier">
                                                            <span class="timar-info"><?php echo  $text_timer; ?></span>
                                                            <div class="timer"
                                                                 data-timeend="<?php echo $product['count_end_date']; ?>"
                                                                 data-timetext="<?php echo str_replace('"','', $product['name']); ?>">

                                                            </div>

                                                        </div>
                                                    <?php } ?>
                                                    <div class="plash top-sales">
                                                        <span>-<?php echo $product['sales']; ?>%</span>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product['news']) { ?>
                                                    <div class="plash new-prod">
                                                        <span>New</span>
                                                    </div>
                                                <?php } ?>


                                            </div>
                                            <a href="<?php echo $product['href']; ?>">
                                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                            </a>
                                            <div class="button-group">
                                                <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" data-dismiss="modal"><?php echo $by_one_click; ?></button>
                                                <a href="<?php echo $product['remove']; ?>" title="<?php echo $button_remove; ?>" class="button remove_wishlist"><i class="fa fa-times"></i></a>
                                                <button class="mini-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>//');"><i class="icon-shopping-purse"></i></button>
                                            </div>
                                        </div>
                                        <div class="caption">
                                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                            <?php if ($product['price']) { ?>
                                                <p class="price">
                                                    <?php if (!$product['special']) { ?>
                                                        <?php echo $product['price']; ?>
                                                    <?php } else { ?>
                                                        <span class="price-new"><?php echo $product['special']; ?></span>
                                                        <span class="price-old"><?php echo $product['price']; ?></span>
                                                    <?php } ?>
                                                </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="col-xs-12">
                                <p><?php echo $text_empty; ?></p>
                                <div class="buttons clearfix">
                                    <div class=""><a href="<?php echo $continue; ?>" class="btn btn-default"><span><?php echo $button_continue; ?></span></a></div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?></div>
    </div>
<?php echo $footer; ?>