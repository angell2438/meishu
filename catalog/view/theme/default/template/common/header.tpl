<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/reject/reject.css" media="all"/>
    <script src="/reject/reject.min.js"></script>
    <![endif]-->
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description;
        if (isset($_GET['page'])) {
            echo " - " . ((int)$_GET['page']) . " " . $text_page;
        } ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title;
    if (isset($_GET['page'])) {
        echo " - " . ((int)$_GET['page']) . " " . $text_page;
    } ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <?php if ($gtm) { ?>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?
                id = '+i+dl;f.parentNode.insertBefore(j,f);
            })(window, document, 'script', 'dataLayer', '<?php echo $gtm; ?>');</script>
        <!-- End Google Tag Manager -->
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/javascript/jquery/slick/slick-theme.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/slick/slick.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/select2.min.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/fancybox/jquery.fancybox.css" rel="stylesheet">
    <!--    <link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet">-->
    <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/default/scss/sergo.css" rel="stylesheet">
    <link href="catalog/view/theme/default/scss/main.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/sergo.css" rel="stylesheet">
<!--    <link href="catalog/view/theme/default/stylesheet/main_information.css" rel="stylesheet">-->

    <link href="catalog/view/theme/default/scss/media.css" rel="stylesheet">
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
              media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/common.js"></script>
    <script src="//npmcdn.com/isotope-layout@3/dist/isotope.pkgd.js"></script>
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if ($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<div class="f_wrapper">
    <div class="f_content">
<header>
    <div class="container">
        <div class="row flex-row flex-row__sm">
            <div class="visible-sm visible-xs col-sm-4 col-xs-3">
                <div class="humburger_wrapp">
                    <div class="humberger_item">

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-9">
                <div class="logo">
                    <?php if ($logo) { ?>
                        <?php if ($home == $og_url) { ?>
                            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"
                                 class="img-responsive"/>
                        <?php } else { ?>
                            <a href="<?php echo $home; ?>">
                                <img src="<?php echo $logo; ?>"
                                     title="<?php echo $name; ?>"
                                     alt="<?php echo $name; ?>" class="img-responsive"/>
                            </a>
                        <?php } ?>
                    <?php } else { ?>
                        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                    <?php } ?>
                </div>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-md-6 col-sm-12 col-xs-12 main_menu_wrapp">
                <div class="row">
                    <div class="wrapper-menu">
                        <?php if ($categories) { ?>
                            <nav id="menu" class="navbar">
                                <div class="navbar-collapse navbar-ex1-collapse">
                                    <ul class="nav navbar-nav">
                                        <?php if ($informations) { ?>
                                            <?php foreach ($informations as $information) { ?>
                                                <li>
                                                    <a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php $k='0'; foreach ($categories as $category) { $k++; ?>
                                            <?php if ($category['children']) { ?>
                                                <li class="dropdown-list collapsed pinck-color with_child_item">
                                                    <a class="list-notactive"
                                                       href="<?= $category['href']; ?>">
                                                        <?php echo $category['name']; ?>
                                                    </a>
                                                    <div class="wrapper-menu-collaps">
                                                        <div class="container">
                                                            <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                                                <?php foreach ($children as $child) { ?>
                                                                    <div class="col-md-4">
                                                                        <div class="child__container">
                                                                            <a href="<?php echo $child['href']; ?>">
                                                                                <img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>">
                                                                                <div class="child__name"><?php echo $child['name']; ?></div>
                                                                            </a>

                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="backdrop"></li>
                                            <?php } else { ?>
                                                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($ncategories) { ?>
                                            <?php $k='1'; foreach ($ncategories as $ncategory) { $k++; ?>
                                                <?php if ($ncategory['children']) { ?>
                                                    <li class="dropdown-list collapsed with_child_item">
                                                        <a class="list-notactive"
                                                           href="<?= $ncategory['href']; ?>">
                                                            <?php echo $ncategory['name']; ?>
                                                        </a>
                                                        <?php if ($ncategory['children']) { ?>
                                                            <div class="wrapper-menu-collaps">
                                                                <div class="container">
                                                                    <?php foreach ($ncategory['children'] as $child) { ?>
                                                                        <div class="col-md-4">
                                                                            <div class="child__container child__container__link">
                                                                                <a href="<?php echo $child['href']; ?>">
                                                                                    <?php echo $child['name']; ?>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </li>
                                                    <li class="backdrop"></li>
                                                <?php } else { ?>
                                                    <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                                    </ul>
                                </div>
                            </nav>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12 xs-border">
                <div class="flex-row mini-position text-right">
                    <div class="search-wrapp">
                        <div class="visible-sm">
                            <a class="search-sm circle-block" href="<?php echo $search_link; ?>">
                                <i class="fa fa-search"></i></a>
                        </div>
                        <div class="hidden-sm hidden-xs">
                            <?php echo $search; ?>
                        </div>
                    </div>
                    <div class="flex-row">
                        <div class="visible-xs">
                            <a class="search-sm circle-block" href="<?php echo $search_link; ?>">
                                <i class="fa fa-search"></i></a>
                        </div>
                        <div class="circle-block wishlist_wrapp<?php echo ($text_wishlist > 0) ? ' has_product' : '' ?>">
                            <a href="<?php echo $wishlist; ?>">
                                <i class="icon-heart" aria-hidden="true"></i>
                                <span class="count_item"><?php echo $text_wishlist; ?></span>
                            </a>
                        </div>
                        <?php echo $cart; ?>
                        <div class="login-block circle-block">
                            <?php if ($logged): ?>
                                <a href="<?= $account ?>" title="<?php echo $text_account; ?>">
                                    <i class="icon-user"></i>
                                </a>
                            <?php else: ?>
                                <a href="" title="<?php echo $text_account; ?>" data-toggle="modal"
                                   data-target="#modal-authorization">
                                    <i class="icon-user"></i>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="xs-menu-block"></div>
    </div>
</header>

