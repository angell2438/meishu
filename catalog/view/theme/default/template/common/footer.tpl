</div> <!--end .f_content in header-->
<div class="f_footer">
<footer>
    <div class="container">
        <div class="row">
            <?php if ($informations) { ?>
                <div class="col-md-3 col-sm-4 parent-elements">
                    <h3 class="footer-title">Meishu <i class="visible-xs fa fa-angle-down" aria-hidden="true"></i></h3>
                    <ul class="list-unstyled hide_xs">
                        <?php foreach ($informations as $information) { ?>
                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>

                        <?php if ($ncategories) { ?>
                            <?php foreach ($ncategories as $ncat) { ?>
                                <li><a href="<?php echo $ncat['href']; ?>"><?php echo $ncat['name']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                    </ul>
                </div>
            <?php } ?>
            <div class="col-md-3 col-sm-4 parent-elements">
                <?php foreach ($categories as $category) { ?>
                    <?php if ($category['children']) { ?>
                        <h3 class="footer-title">
                            <?php echo $category['name']; ?>
                            <i class="visible-xs fa fa-angle-down" aria-hidden="true"></i>
                        </h3>

                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                            <ul class="list-unstyled hide_xs">
                                <?php foreach ($children as $child) { ?>
                                    <li>
                                        <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

            </div>
            <div class="col-md-3 col-sm-4 parent-elements">
                <h3 class="footer-title"><?php echo $text_contact; ?> <i class="visible-xs fa fa-angle-down" aria-hidden="true"></i></h3>
                <div class="hide_xs">
                    <ul class="list-unstyled">
                        <li>
                            <a class="phone-shop" href="mailto:<?php echo $email; ?>">
                                <?php echo $email; ?>
                            </a>
                        </li>
                        <li>
                            <a class="phone-shop" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                                <?php echo $telephone; ?>
                            </a>
                        </li>

                    </ul>
                    <ul class="social clearfix">
                        <li><a href="<?php echo $config_fb; ?>" target="_blank"><i class="icon-facebook"></i></a></li>
                        <li><a href="<?php echo $config_inst; ?>" target="_blank"><i class="icon-instagram"></i></a></li>
                        <li><a href="<?php echo $config_twitter; ?>" target="_blank"><i class="icon-twitter"></i></a></li>
                        <li><a href="<?php echo $config_you; ?>" target="_blank"><i class="icon-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix visible-sm"></div>
            <div class="col-md-3 col-sm-12 parent-elements footer_subscribe">
                <h3 class="footer-title">Рассылка <i class="visible-xs fa fa-angle-down" aria-hidden="true"></i></h3>
                <?php echo $newsletter; ?>
            </div>
        </div>
    </div>
</footer>
<div class="copiright">
    <div class="container">
        <div class="row flex-row flex-column">
            <div class="col-md-10 col-sm-12">
                <div class="copiright-text">
                    <p><?php echo $powered; ?></p><a href="<?= $link_privacy; ?>">Политика Конфиденциальности</a>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <a href="http://web-systems.solutions/" target="_blank">
                    <img src="/catalog/view/theme/default/image/copyright.svg" alt="web-systems.solutions" title="web-systems.solutions" width="151" height="19">
                </a>
            </div>
        </div>
    </div>
</div>
</div>


<?php echo $quicksignup; ?>

<div class="modal fade" id="thanks_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <h2 class="modal-title"></h2>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<div class="controll_contaien hidden"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>

<script src="catalog/view/javascript/jquery/slick/slick.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/jquery/select2.min.js"></script>
<script src="catalog/view/javascript/jquery/machheigh.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="https://tympanus.net/TipsTricks/DirectionAwareHoverEffect/js/modernizr.custom.97074.js"></script>
<script src="catalog/view/javascript/jquery/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<!--<script src="catalog/view/javascript/masonry.pkgd.min.js" type="text/javascript"></script>-->
<script src="catalog/view/javascript/dotdotdot.min.js"></script>
<script src="catalog/view/javascript/main.js"></script>
</div> <!--end .f_wrapper in header-->

</body></html>