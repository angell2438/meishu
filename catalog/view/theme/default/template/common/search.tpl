<div id="search" class="input-group search_container">
    <div class="header_search circle-block">
        <span class="icon icon-search"></span>
    </div><!-- end header_search -->

    <div class="search_block_content">
        <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" id="input-search" />
    </div><!-- end search_block_content -->

</div><!-- end search -->
