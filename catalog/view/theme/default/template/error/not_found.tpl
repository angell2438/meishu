<?php echo $header; ?>
    <div class="erorrs-wrapper">
        <div class="container">
            <div class="erorrs-text">
                <h1 class="hidden"><?php echo $heading_title; ?></h1>
                <img src="/catalog/view/theme/default/image/404.svg" alt="404 page">
                <p><?php echo $text_error; ?></p>
                <div class="buttons">
                    <a href="<?php echo $continue; ?>" class="btn btn-slider">
                       На главную
                    </a>
                </div>
            </div>

        </div>
    </div>
<?php echo $footer; ?>