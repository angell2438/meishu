<div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
        <div class="image">
            <div class="sale-label">
                <?php if ($product['sales']) { ?>
                    <div class="plash top-sales">
                        <span> - <?php echo $product['sales']; ?>%</span>
                    </div>
                <?php } ?>
                <?php if ($product['news']) { ?>
                    <div class="plash new-prod">
                        <span>New</span>
                    </div>
                <?php } ?>
            </div>
            <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>"
                     alt="<?php echo $product['name']; ?>"
                     title="<?php echo $product['name']; ?>" class="img-responsive" />
            </a>
            <div class="button-group">
                <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" ><?php echo $by_one_click; ?></button>
                <?php if ($product['wishlist_status']){ ?>
                    <button class="mini-btn wishlist-js active" type="button" ><i class="icon-heart"></i></button>
                <?php } else { ?>
                    <button class="mini-btn wishlist-js" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                        <i class="icon-heart"></i></button>
                <?php } ?>
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-shopping-purse"></i></button>
            </div>
        </div>
        <div class="caption">
            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                    <?php if ($product['tax']) { ?>
                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>

    </div>
</div>