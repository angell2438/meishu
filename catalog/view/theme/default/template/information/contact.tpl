<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
        <div class="block-information-contact">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contact-info_big js-height">
                        <p>номер телефона</p>
                        <div class="flex-row">
                            <?php if ($telephone) { ?>

                                <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                                    <?php echo $telephone; ?>
                                </a>
                            <?php } ?>
                            <?php if ($fax) { ?>
                                <a  href="tel:<?php echo preg_replace("/[ ()-]/", "", $fax) ;?>">
                                    <span class="hidden-sm hidden-xs">/</span> <?php echo $fax; ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="contact-info_big js-height">
                        <p>электронная почта </p>
                            <?php if ($mail) { ?>
                                <a href="mailto:<?php echo  $mail; ?>">
                                    <?php echo $mail; ?>
                                </a>
                            <?php } ?>





                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="contact-info_big js-height">
                        <p> whatsapp</p>
                        <?php if ($whatsapp) { ?>
                            <a href="whatsapp://send?phone=<?php echo preg_replace("/[ ()-]/", "", $whatsapp) ;?>">
                                <?php echo $whatsapp; ?>
                            </a>
                        <?php } ?>

                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="contact-info_big js-height">
                        <p>skype</p>
                        <?php if ($skype) { ?>
                            <a href="skype:[<?php echo  $skype; ?>]?call:">
                                <?php echo $skype; ?>
                            </a>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>

        <div class="c-form-style">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal white-style-form">

                <h2><?php echo $text_contact; ?></h2>
                <div class="clearfix">
                    <div class="col-md-4">
                        <div class="form-group required">
                            <input type="text" name="name" value="<?php echo $name; ?>" id="input-name_contact" class="form-control" placeholder="<?php echo $entry_name; ?>"/>
                            <?php if ($error_name) { ?>
                                <div class="text-danger"><?php echo $error_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required">
                            <input type="text" name="email" value="<?php echo $email; ?>" id="input-email_contact" class="form-control"  placeholder="<?php echo $entry_email; ?>"/>
                            <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required">
                            <input type="text" name="phone" value="<?php echo $phone; ?>" id="input-phone_contact" class="form-control"  placeholder="<?php echo $entry_phone; ?>"/>

                        </div>
                    </div>
                </div>


                <div class="form-group required">
                    <div class="col-sm-12">
                        <textarea name="enquiry" rows="10" id="input-enquiry_contact" class="form-control" placeholder="<?php echo $entry_enquiry; ?>"></textarea>
                        <?php if ($error_enquiry) { ?>
                            <div class="text-danger"><?php echo $error_enquiry; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group style-input required">
                        <div class="privacy_policy">
                            <label class="form-checkbox" for="privacy_policy">
                                <input id="privacy_policy" name="privacy_policy" value="1" type="checkbox" <?php if ($privacy_policy) { ?> checked<?php } ?>>
                                <span class="form-checkbox__marker"></span>
                                <span class="form-checkbox__label">Согласие с  </span>
                            </label>
                            <a target="_blank" href="<?php  echo $link_privacy; ?>"> Политикой Конфиденциальности</a>
                            <?php if ($error_privacy_policy) { ?>
                                <div class="text-danger">Вы должны согласиться с правилами конфиденциальности</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>


                <div class="buttons c-center clearfix">
                    <input class="btn btn-default orang" type="submit" value="<?php echo $button_send; ?>" />

                </div>
            </form>
        </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
