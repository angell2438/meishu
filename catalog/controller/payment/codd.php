<?php
class ControllerPaymentCodd extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/codd.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/codd.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/codd.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'codd') {
			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('coda_order_status_id'));
		}
	}
}
